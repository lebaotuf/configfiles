;;; --------		   GC			--------
(setq gc-cons-threshold 100000000)

;;; --------		PACKAGES		--------
;; Enable melpa
(require 'package)
(setq package-archives
      '(("GNU ELPA" . "http://elpa.gnu.org/packages/")
	("MELPA Stable" . "http://stable.melpa.org/packages/")
	("MELPA" . "https://melpa.org/packages/"))
      package-archive-priorities
      '(("MELPA Stable" . 10)
        ("GNU ELPA" . 5)
        ("MELPA" . 0)))

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package which-key
  :ensure t
  :init
  (which-key-mode))

(use-package shift-number
  :ensure t)

(use-package tex
  :ensure auctex)

(use-package undo-tree
  :ensure t
  :config
  (setq undo-tree-auto-save-history t)
  (setq undo-tree-history-directory-alist '(("." . "~/.emacs.d/undo")))
  (global-undo-tree-mode))

(use-package company
  :ensure t
  :init
  (add-hook 'after-init-hook 'global-company-mode)
  (add-hook 'c++-mode-hook 'disable-company-semantic)
  (setq company-idle-delay 0)
  (setq company-clang-insert-arguments nil)
  (setq company-show-numbers t))

(use-package diminish
  :ensure t
  :config
  (diminish 'which-key-mode)
  (diminish 'company-mode)
  (diminish 'undo-tree-mode))

(use-package htmlize
  :ensure t)

(use-package vc-fossil
  :ensure t
  :config (add-to-list 'vc-handled-backends 'Fossil t))

(use-package lsp-mode
  :ensure t
  :hook (c-mode . lsp)
  :hook (c++-mode . lsp)
  :commands lsp
  :init
  (setq lsp-enable-snippet nil))

(use-package company-lsp
  :ensure t
  :commands company-lsp)

(use-package elfeed
  :ensure t
  :init
  (setq elfeed-db-directory (expand-file-name "~/.emacs.d/elfeed/")))

(use-package spacemacs-theme
  :defer t
  :init (load-theme 'spacemacs-light 'no-confirm))

(use-package web-mode
  :ensure t
  :mode
  (("\\.html\\'" . web-mode)
   ("\\.js[x]?\\'" . web-mode)))
;;; --------		END PACKAGES		--------


;;; --------		USABILITY SETTINGS	--------
;; Increase image resolution for docview
(require 'doc-view)
(setq doc-view-resolution 144)

;; Use zsh by default in ansi-term
(defvar my-term-shell "/usr/bin/zsh")
(defadvice ansi-term (before force-zsh)
  (interactive (list my-term-shell)))
(ad-activate 'ansi-term)

;; Use /tmp as temp backup file directory
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Kill whole word
(defun kill-whole-word ()
  "Kill the whole word under cursor"
  (interactive)
  (backward-word)
  (kill-word 1))

;;Kill whole symbol
(defun kill-whole-sexp ()
  "Kill the whole symbolic expression under cursor"
  (interactive)
  (backward-sexp)
  (kill-sexp 1))

;; Kill help window and buffer
(defun kill-and-delete-help ()
  "Kill *Help* buffer and close its window"
  (interactive)
  (let ((window)
	(buffer))
    (setq buffer (get-buffer "*Help*"))
    (when buffer
      (setq window (get-buffer-window buffer))
      (kill-buffer buffer))
    (when window
      (delete-window window))))

(defun kill-other-window-and-buffer ()
  "Kill the next window and the buffer in it"
  (interactive)
  (let ((win-curr (selected-window))
	(win-other (next-window)))
    (select-window win-other)
    (kill-this-buffer)
    (delete-window win-other)
    (select-window win-curr)))

(defun toggle-maximize-buffer ()
  "Maximize buffer or restore window configuration"
  (interactive)
  (if (= 1 (length (window-list)))
      (jump-to-register '_)
    (progn
      (window-configuration-to-register '_)
      (delete-other-windows))))

;; Sh-mode variable skeleton
(require 'sh-script)
(define-skeleton sh-quoted-var
  "Insert a quoted variable expression."
  (sh "Variable: "
      > "\"${" str "}\""))

;; C-mode basic file skeleton
(require 'cc-mode)
(define-skeleton c-basic-file
  "Insert a basic file skeleton"
  nil
  "/* -*- mode: C; compile-command: \"gcc -Wall -Wextra -pedantic -O2 "
  (file-name-nondirectory buffer-file-name)
  " -o /tmp/"
  (file-name-nondirectory (file-name-sans-extension buffer-file-name))
  "; /tmp/"
  (file-name-nondirectory (file-name-sans-extension buffer-file-name))
  "\" -*- */\n"
  "#include <stdio.h>\n"
  "#include <stdlib.h>\n"
  "\n"
  "int main(void){\n"
  _ "\n"
  "}\n")

(define-skeleton c++-basic-file
  "Insert a basic file skeleton"
  nil
  "/* -*- mode: C++; compile-command: \"g++ -Wall -Wextra -pedantic -O2 "
  (file-name-nondirectory buffer-file-name)
  " -o /tmp/"
  (file-name-nondirectory (file-name-sans-extension buffer-file-name))
  "; /tmp/"
  (file-name-nondirectory (file-name-sans-extension buffer-file-name))
  "\" -*- */\n"
  "#include <iostream>\n\n"
  "//using namespace std;\n"
  "\n"
  "int main(void){\n"
  _ "\n"
  "}\n")

(defconst my-cc-style
  '("cc-mode"
    (c-offsets-alist . ((innamespace . [0])))))

(c-add-style "my-cc-mode" my-cc-style)

(setq c-default-style (append c-default-style '((c++-mode . "my-cc-mode"))))

;; Sh-mode command substitution skeleton
(define-skeleton sh-command-subs
  "Insert a command substitution."
  (sh "Command: "
      > "\"$(" str ")\""))

;; Calendar customisation
(require 'calendar)
(setq calendar-week-start-day 1
      calendar-intermonth-text
        '(propertize
          (format "%2d"
                  (car
                   (calendar-iso-from-absolute
                    (calendar-absolute-from-gregorian (list month day year)))))
          'font-lock-face 'font-lock-function-name-face))

(setq holiday-christian-holidays
      '((holiday-fixed 12 25 "Karácsony")
	(holiday-fixed 12 26 "Karácsony")
	(holiday-easter-etc -2 "Nagypéntek")
	(holiday-easter-etc 0 "Húsvét vasárnap")
	(holiday-easter-etc +1 "Húsvét hétfő")
	(holiday-easter-etc +49 "Pünkösd vasárnap")
	(holiday-easter-etc +50 "Pünkösd hétfő")
	(holiday-fixed 11 1 "Mindenszentek")
	(if calendar-christian-all-holidays-flag
	    (append
	     (holiday-fixed 1 6 "Vízkereszt")
	     (holiday-easter-etc -47 "Húshagyókedd")
	     (holiday-easter-etc -46 "Hamvazószerda")
	     (holiday-easter-etc -7 "Virágvasárnap")
	     (holiday-easter-etc -1 "Nagyszombat")
	     (holiday-fixed 11 2 "Halottak napja")
	     (holiday-fixed 12 24 "Szenteste")
	     (holiday-float 12 0 -4 "Advent első vasárnapja" 24)
	     (holiday-float 12 0 -3 "Advent második vasárnapja" 24)
	     (holiday-float 12 0 -2 "Advent harmadik vasárnapja" 24)
	     (holiday-float 12 0 -1 "Advent negyedik vasárnapja" 24)))))

(setq holiday-general-holidays
      '((holiday-fixed 1 1 "Újév")
	(holiday-fixed 3 15 "1848-49-es forradalom és szabadságharc")
	(holiday-fixed 5 1 "Munka ünnepe")
	(holiday-float 5 0 1 "Anyák napja")
	(holiday-fixed 8 20 "Államalapítás")
	(holiday-fixed 10 23 "1956-os forradalom")))

(setq calendar-holidays (append
			 holiday-general-holidays
			 holiday-local-holidays
			 holiday-other-holidays
			 holiday-christian-holidays))

;;		    :font "-xos4-terminus-medium-r-normal-*-16-*-*-*-*-*-iso8859-*")
;;; --------		END USABILITY SET.	--------


;;; --------		MODE SETTINGS		--------
;; Paren-mode customisation
(require 'paren)
(set-face-background 'show-paren-match "#9400d3")
(set-face-foreground 'show-paren-match "#def")
(setq show-paren-delay 0)
(show-paren-mode 1)

(require 'whitespace)
(setq whitespace-style '(face trailing lines-tail))
(add-hook 'prog-mode-hook 'whitespace-mode)

(require 'ido)
(ido-mode 'buffers)
(setq ido-separator "\n")

;; Dired mode
(defun set-lang-to-en-us ()
  "Set the LANG environment variable to en_US.UTF-8"
  (setenv "LANG" "en_US.UTF-8"))
(add-hook 'dired-mode-hook 'set-lang-to-en-us)

;; Company mode helper
(defun disable-company-semantic ()
  "Remove company-semantic from company-backends list"
  (make-local-variable 'company-backends)
  (setq company-backends (delete 'company-semantic company-backends)))

;; Org mode settings
(setq org-agenda-files '("~/Dokumentumok/org/TODO.org"
			 "~/PEgy/egyetem.org"))
(setq org-agenda-window-setup 'current-window)
(setq org-refile-targets '(("TODO.org" :maxlevel . 2)))
(setq org-refile-use-outline-path t)

;; Display line numbers in prog modes
(add-hook 'prog-mode-hook
	  (lambda ()
	    (setq display-line-numbers t)))

;; Save minibuffer history
(savehist-mode 1)

;; Disable blinking cursor
(blink-cursor-mode 0)

;; Save point in file
(save-place-mode 1)
;;; --------		END MODE SETTINGS	--------

;;; --------		LOOK SETTINGS		--------
;; Set face attributes after making frame
(add-hook 'after-make-frame-functions
	  (lambda (frame)
	    (select-frame frame)
	    (if (featurep 'cairo)
		(set-fontset-font t
				  'symbol
				  (font-spec :family "Noto Color Emoji")
				  frame
				  'prepend))
	    (set-face-attribute 'company-tooltip frame
	    			:background "powder blue")
	    (set-face-attribute 'company-tooltip-selection frame
	    			:background "DeepSkyBlue3")
	    (set-face-attribute 'company-scrollbar-bg frame
	    			:background "steel blue")
	    (set-face-attribute 'company-scrollbar-fg frame
	    			:background "DeepSkyBlue4")
	    (set-face-attribute 'company-tooltip-common-selection frame
	    			:background "cyan3")
	    (set-face-attribute 'line-number-current-line frame
				:weight 'bold)
	    (set-face-attribute 'default frame
				:family "Envy Code R"
				:height 105)
	    (set-face-attribute 'region frame
	     			:background "#e8e8e8")))

;; Disable menu bar and tool bar
(menu-bar-mode -1)
(tool-bar-mode -1)

;; Modeline
(line-number-mode 1)
(column-number-mode 1)

;; Highlight certain keywords
(defun highlight-todos ()
  "Highlight TODOs and other stuff."
  (font-lock-add-keywords nil
			  '(("\\b\\(FIX\\(ME\\)?\\|TODO\\|XXX\\|BUG\\)\\b"
			     1 font-lock-warning-face t))))
(add-hook 'prog-mode-hook 'highlight-todos)

;;; --------		END LOOK SETTINGS	--------


;;; --------		MINOR SETTINGS		--------
;; Octave mode for .m files
(setq auto-mode-alist
      (cons '("PKGBUILD" . sh-mode)
      (cons '("\\.m$" . octave-mode) auto-mode-alist)))

;; Etc.
(setq ispell-program-name "/usr/bin/hunspell"
      ispell-local-dictionary "hu_HU"
      ispell-local-dictionary-alist '(("hu_HU" "\[\[:alpha:\]\]" "[^[:alpha:]]" "[']" nil ("-d" "hu_HU") nil utf-8)))
(setq ispell-dictionary "hu_HU")

(setq tramp-default-method "ssh")

(setq inhibit-startup-screen t)

(setq scheme-program-name "guile")

(setq LaTeX-electric-left-right-brace t)

(setq visible-bell t)

(setq frame-resize-pixelwise t)

(defun toggle-var (var)
  (set var (not (eval var))))
;;; --------		END MINOR SETTINGS	--------


;;; --------		KEYBINDINGS		--------
(windmove-default-keybindings)
; Join line
(global-set-key (kbd "C-c j") 'join-line)
; Increment/decrement number
(global-set-key (kbd "C-c +") 'shift-number-up)
(global-set-key (kbd "C-c -") 'shift-number-down)
;; Use buffer-menu so I don't have to change windows
(global-set-key (kbd "C-x C-b") 'ibuffer)
;; Kill buffer without question if saved
(global-set-key (kbd "C-x k") (lambda () (interactive) (kill-buffer (current-buffer))))
;; Kill whole word
(global-set-key (kbd "C-c w w") 'kill-whole-word)
;; Kill whole symbol
(global-set-key (kbd "C-c w s") 'kill-whole-sexp)
;; Kill help window and buffer
(global-set-key (kbd "C-c w h") 'kill-and-delete-help)
(global-set-key (kbd "C-c w q") 'kill-other-window-and-buffer)
(global-set-key (kbd "C-x 4 k")
		(lambda () (interactive)
		  (kill-buffer (window-buffer (next-window)))))
; Launch things
(global-set-key (kbd "C-c l c") 'calc)
(global-set-key (kbd "C-c l w") 'eww)
(global-set-key (kbd "C-c l e") 'eshell)
(global-set-key (kbd "C-c l p") 'list-packages)
(global-set-key (kbd "C-c l o") 'occur)
(global-set-key (kbd "C-c l i")
		(lambda () (interactive) (find-file (file-truename "~/.emacs.d/init.el"))))
(global-set-key (kbd "C-c l a") 'org-agenda-list)
(global-set-key (kbd "C-c l t")
		(lambda () (interactive) (find-file (file-truename "~/Dokumentumok/org/TODO.org"))))
(global-set-key (kbd "C-c l u")
		(lambda () (interactive) (find-file (file-truename "~/PEgy/egyetem.org"))))
(global-set-key (kbd "C-c l n") 'calendar)
(global-set-key (kbd "C-c l f") 'elfeed)
; Holiday settings
(define-key calendar-mode-map "C" (lambda ()(interactive)
				     (toggle-var 'calendar-christian-all-holidays-flag)
				     (calendar-unmark)
				     (calendar-mark-holidays)))

(define-key sh-mode-map (kbd "C-c C-v") 'sh-quoted-var)
(define-key sh-mode-map (kbd "C-c C-e") 'sh-command-subs)
(define-key c-mode-map (kbd "<f5>") 'compile)
(define-key c++-mode-map (kbd "<f5>") 'compile)

(which-key-add-key-based-replacements "C-c l i" "open-init-file")
(which-key-add-key-based-replacements "C-c l t" "open-todo-file")
(which-key-add-key-based-replacements "C-c l u" "open-uni-org-file")
(which-key-add-key-based-replacements "C-x 4 k" "kill-buffer-in-next-window")
(which-key-add-major-mode-key-based-replacements 'calendar-mode "C" "toggle-all-christian-holidays")
;;; --------		END KEYBINDINGS		--------

;; Load private configuration (eg. feeds)
(let ((personal "~/.emacs.d/personal.el"))
  (when (file-exists-p (expand-file-name personal))
    (load personal)))


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(web-mode spacemacs-theme elfeed company-lsp lsp-mode which-key vc-fossil use-package undo-tree shift-number mines htmlize diminish company auctex 2048-game)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
