set backup
set undofile
set backupdir=~/.vim/vimbackup//
set undodir=~/.vim/vimundo//

highlight ExtraWhitespace ctermbg=red guibg=red
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/

highlight Normal term=none cterm=none ctermfg=White ctermbg=Black gui=none guifg=White guibg=Black
highlight DiffAdd cterm=none ctermfg=fg ctermbg=Blue gui=none guifg=fg guibg=Blue
highlight DiffDelete cterm=none ctermfg=fg ctermbg=Blue gui=none guifg=fg guibg=Blue
highlight DiffChange cterm=none ctermfg=fg ctermbg=Blue gui=none guifg=fg guibg=Blue
highlight DiffText cterm=none ctermfg=bg ctermbg=Green gui=none guifg=bg guibg=Green

set statusline=\ %#StatusLineBold#\ %m%t\ %*\ %l/%L\ %=[%{&ff},%{&ft}]\ [a=\%03.3b]\ [%l,%v]
set laststatus=2
highlight StatusLine term=none cterm=none ctermfg=fg ctermbg=DarkBlue
highlight StatusLineBold term=none cterm=none ctermfg=Yellow ctermbg=Black
highlight StatusLineNC term=none cterm=none ctermfg=DarkBlue ctermbg=White

filetype plugin on
let mapleader=","
set timeout timeoutlen=1500
"set term=xterm-256color

set title
set titlestring=vim:\ %-25.55F\ %a%r%m titlelen=70

map <leader>h :wincmd h<CR>
map <leader>j :wincmd j<CR>
map <leader>k :wincmd k<CR>
map <leader>l :wincmd l<CR>

call plug#begin('~/.vim/plugged')

Plug 'rust-lang/rust.vim'

Plug 'scrooloose/nerdcommenter'

Plug 'hari-rangarajan/CCTree'

Plug 'Raimondi/delimitMate'

call plug#end()

